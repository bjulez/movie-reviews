// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

var api_key = 'b9f998beb8f04fd61747cb6e292c7da1';
function generateMovieList() {
    $(document).ready(function(){
        $.ajax({
            url: 'http://api.themoviedb.org/3/movie/popular?api_key=' + api_key,
            dataType: 'jsonp',
            jsonpCallback: 'testing'
        }).error(function() {
            console.log('error')
        }).done(function(response) {
            var body = document.body;                               //body = document body
            var movietable = document.createElement('table');       //movietable = new table
            movietable.className = "sortable";                      //movietable class = sortable

            for (var i = 0; i < response.results.length; i++) {
                var tr = movietable.insertRow();
                var td = tr.insertCell();
                td.href = "http://google.com";
                td.appendChild(document.createTextNode(response.results[i].title));
                var td = tr.insertCell();
                td.appendChild(document.createTextNode(response.results[i].release_date));
                var td = tr.insertCell()
                td.appendChild(document.createTextNode(response.results[i].genre_ids));
            }
            var header = movietable.createTHead();                  //header = new header
            var htr = header.insertRow(0);
            var name = htr.insertCell(0);
            var date = htr.insertCell(1);
            var genre = htr.insertCell(2);
            name.innerHTML = "Movie Title";
            date.innerHTML = "Release Date";
            genre.innerHTML = "Genre";

            body.appendChild(movietable);
            console.log(response);      //remove when finished
            console.log(movietable);    //remove when finished

    });
});
}

function getAjaxMovies() {
    $.ajax({
        url: 'http://api.themoviedb.org/3/movie/popular?api_key=' + api_key,
        dataType: 'jsonp',
        jsonpCallback: 'testing'
    }).error(function() {
        console.log('error')
    }).done(function(response) {
       console.log(response);
    });
}