# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Movie.create!(name: 'Jurassic World'.downcase.parameterize)
Movie.create!(name: 'The Martian'.downcase.parameterize)
Movie.create!(name: 'Terminator Genisys'.downcase.parameterize)
Movie.create!(name: 'Mad Max: Fury Road'.downcase.parameterize)
Movie.create!(name: 'San Andreas'.downcase.parameterize)
Movie.create!(name: 'Tomorrowland'.downcase.parameterize)
Movie.create!(name: 'Avengers: Age of Ultron'.downcase.parameterize)
Movie.create!(name: 'Southpaw'.downcase.parameterize)
Movie.create!(name: 'Minions'.downcase.parameterize)
Movie.create!(name: 'Pixels'.downcase.parameterize)
Movie.create!(name: 'Inside Out'.downcase.parameterize)
Movie.create!(name: 'Interstellar'.downcase.parameterize)
Movie.create!(name: 'Ant-Man'.downcase.parameterize)
Movie.create!(name: 'Maze Runner: The Scorch Trials'.downcase.parameterize)
Movie.create!(name: 'The Hunger Games: Mockingjay - Part 1'.downcase.parameterize)
Movie.create!(name: 'Kingsman: The Secret Service'.downcase.parameterize)
Movie.create!(name: 'The Hobbit: The Battle of the Five Armies'.downcase.parameterize)
Movie.create!(name: 'Hotel Transylvania 2'.downcase.parameterize)
Movie.create!(name: 'Big Hero 6'.downcase.parameterize)
Movie.create!(name: 'Insurgent'.downcase.parameterize)